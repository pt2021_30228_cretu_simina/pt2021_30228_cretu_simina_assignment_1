package ro.tuc.tp;
import java.util.ArrayList;
import java.util.List;



public class Polinom {

    List <Monom> polinom= new ArrayList<Monom>();
    private Monom m1;
    private Monom m2;
    private Monom m3;

    public Polinom() { }

    public Polinom adunare(Polinom a) {
        Polinom polinom= new Polinom();
        m1=new Monom(0, 0);
        m2=new Monom(0, 0);
        m3=new Monom(0, 0);
        int i=0,j=0;

        while(i < this.polinom.size() && j < a.polinom.size()) {
            if (this.polinom.get(i).getGrad() > a.polinom.get(j).getGrad()) {
                polinom.polinom.add(this.polinom.get(i));
                i++;
            } else {
                if (this.polinom.get(i).getGrad() < a.polinom.get(j).getGrad()) {
                    polinom.polinom.add(a.polinom.get(j));
                    j++;
                } else {
                    m3=m3.aduna(this.polinom.get(i), a.polinom.get(j));
                    polinom.polinom.add(m3);
                    i++; j++;
                }
            }
        }
        while(i < this.polinom.size()) {
            polinom.polinom.add(this.polinom.get(i));
            i++;
        }
        while (j < a.polinom.size()) {
            polinom.polinom.add(a.polinom.get(j));
            j++;
        }
        return polinom;
    }

    public Polinom scadere(Polinom a) {
        Polinom polinom= new Polinom();
        m1=new Monom(0, 0);
        m2=new Monom(0, 0);
        m3=new Monom(0, 0);
        int i=0, j=0;

        while(i < this.polinom.size() && j < a.polinom.size()) {
            if (this.polinom.get(i).getGrad() > a.polinom.get(j).getGrad()) {
                polinom.polinom.add(this.polinom.get(i));
                i++;
            } else {
                if (this.polinom.get(i).getGrad() < a.polinom.get(j).getGrad()) {
                    a.polinom.get(j).coeficient=-a.polinom.get(j).getCoeficient();
                    polinom.polinom.add(a.polinom.get(j));
                    j++;
                } else {
                    m3=m3.scade(this.polinom.get(i), a.polinom.get(j));
                    polinom.polinom.add(m3);
                    i++; j++;
                }
            }
        }
        while(i < this.polinom.size()) {
            polinom.polinom.add(this.polinom.get(i));
            i++;
        }
        while (j < a.polinom.size()) {
            a.polinom.get(j).coeficient=-a.polinom.get(j).getCoeficient();
            polinom.polinom.add(a.polinom.get(j));
            j++;
        }
        return polinom;
    }


    public Polinom inmultire(Polinom p) {
        m1=new Monom(0, 0);
        Polinom polinom =new Polinom();

        for (Monom monom1:this.polinom) {
            Polinom polinom2 =new Polinom();
            for (Monom monom2:p.polinom) {
                m1=m1.inmulteste(monom1,
                        monom2);
                polinom2.polinom.add(m1);
            }
            polinom=polinom.adunare(polinom2);
        }
        return polinom;
    }

    public Polinom deriveaza(Polinom p){
        for( int i=0; i < p.polinom.size(); i++) {
            Monom m = p.polinom.get(i);
            if (m.getGrad() != 0) {
                m.setCoeficient(m.getCoeficient() * m.getGrad());
                m.setGrad(m.getGrad() - 1);
            } else {
                p.polinom.remove(m);
            }
        }
        return p;
    }

    public static Polinom integreaza (Polinom p) {
        for( int i=0; i < p.polinom.size(); i++) {
            Monom m = p.polinom.get(i);
                m.setGrad(m.getGrad() + 1);
                m.setCoeficient(m.getCoeficient() / m.getGrad());
        }
        return p;
    }

    public String toString() {
        String string = "";
        for(int i = 0; i < polinom.size(); i++)
            string += polinom.get(i).toString();
        return string;
    }
}