package ro.tuc.tp;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class View extends JFrame{

    private javax.swing.JTextField polinom1;
    private javax.swing.JLabel p1;
    private javax.swing.JTextField polinom2;
    private javax.swing.JLabel p2;
    private javax.swing.JTextField rezultat;
    private javax.swing.JLabel r;
    private javax.swing.JButton adunare;
    private javax.swing.JButton scadere;
    private javax.swing.JButton inmultire;
    private javax.swing.JButton derivare;
    private javax.swing.JButton integrare ;
    private javax.swing.JFrame frame;

    public View(){
        polinom1 = new javax.swing.JTextField(20);
        p1 = new javax.swing.JLabel();
        polinom2 = new javax.swing.JTextField(20);
        p2 = new javax.swing.JLabel();
        rezultat = new javax.swing.JTextField(40);
        r = new javax.swing.JLabel();
        adunare = new javax.swing.JButton();
        scadere = new javax.swing.JButton();
        inmultire = new javax.swing.JButton();
        derivare = new javax.swing.JButton();
        integrare = new javax.swing.JButton();
        frame = new javax.swing.JFrame();

        frame.setSize(550, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Calculator de polinoame");
        frame.setVisible(true);
        frame.setLayout(null);
        frame.add(p1);
        frame.add(p2);
        frame.add(r);
        frame.add(polinom1);
        frame.add(polinom2);
        frame.add(rezultat);
        frame.add(adunare);
        frame.add(scadere);
        frame.add(inmultire);
        frame.add(derivare);
        frame.add(integrare);

        p1.setText("Polinom 1");
        p1.setBounds (60,1, 100,30);
        p2.setText("Polinom 2");
        p2.setBounds (60, 65, 100, 30);
        r.setText("Rezultat");
        r.setBounds (70, 105, 100, 30);
        adunare.setBounds (85, 35, 42, 23);
        scadere.setBounds (135, 35, 40, 23);
        inmultire.setBounds (185, 35, 40, 23);
        derivare.setBounds (235,35,100,23);
        integrare.setBounds (345,35,100,23);
        polinom1.setBounds (135, 6, 152, 23);
        polinom2.setBounds (135, 70, 152, 23);
        rezultat.setBounds (135, 110, 300, 23);

        adunare.setText("+");
        adunare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Polinom a = new Polinom();
                a = model(polinom1.getText());
                Polinom b = new Polinom();
                b = model(polinom2.getText());
                rezultat.setText(a.adunare(b).toString());
            }
        });

        scadere.setText("-");
        scadere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Polinom a = new Polinom();
                a = model(polinom1.getText());
                Polinom b = new Polinom();
                b = model(polinom2.getText());
                rezultat.setText(a.scadere(b).toString());
            }
        });

        inmultire.setText("*");
        inmultire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Polinom a = new Polinom();
                a = model(polinom1.getText());
                Polinom b = new Polinom();
                b = model(polinom2.getText());
                rezultat.setText(a.inmultire(b).toString());
            }
        });

        derivare.setText("derivare");
        derivare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Polinom a = new Polinom();
                a = model(polinom1.getText());
                rezultat.setText(a.deriveaza(a).toString());
            }
        });

        integrare.setText("integrare");
        integrare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Polinom a = new Polinom();
                a = model(polinom1.getText());
                rezultat.setText(a.integreaza(a).toString());
            }
        });
    }

    public Polinom model(String s) {
        Polinom p = new Polinom();
        Pattern pat1 = Pattern.compile("([+-]?[\\d\\.]*[a-zA-Z]?\\^?\\d*)");
        Matcher mat1 = pat1.matcher(s);

        while (!mat1.hitEnd()) {
            mat1.find();
            Pattern pat2 = Pattern.compile("([+-]?[\\d\\.]*)([a-zA-Z]?)\\^?(\\d*)");
            Matcher mat2 = pat2.matcher(mat1.group());
            double coef = 0, grad = 0;
            if (mat2.find()) {
                coef = Double.valueOf(mat2.group(1));
                grad = Double.valueOf(mat2.group(3));
            }
            Monom m = new Monom(coef, grad);
            p.polinom.add(m);
        }
        return p;
    }
}