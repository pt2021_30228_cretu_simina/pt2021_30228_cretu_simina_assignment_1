package ro.tuc.tp;

public class Monom {
    public double coeficient;
    public double grad;

    public Monom() {}
    public Monom(double coeficient, double grad) {
        this.coeficient=coeficient;
        this.grad=grad;
    }
    public double getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(double coeficient) {
        this.coeficient=coeficient;
    }

    public double getGrad() {
        return grad;
    }

    public void setGrad(double grad) { this.grad=grad; }

    private Monom monom;

    public Monom aduna(Monom m1, Monom m2) {
        monom= new Monom();
        monom.setCoeficient(m1.coeficient + m2.coeficient);
        monom.setGrad(m1.grad);
        return monom;
    }
    public Monom scade(Monom m1, Monom m2){
        monom= new Monom();
        monom.setCoeficient(m1.coeficient - m2.coeficient);
        monom.setGrad(m1.grad);
        return monom;
    }
    public Monom inmulteste(Monom m1, Monom m2) {
        monom= new Monom();
        monom.setCoeficient(m1.coeficient * m2.coeficient);
        monom.setGrad(m1.grad + m2.grad);
        return monom;
    }

    public String toString() {
        if (coeficient >= 0)
            return "+" + coeficient + "x^" + (int)grad + " ";
        else
            return coeficient + "x^" + (int)grad + " ";
    }
}